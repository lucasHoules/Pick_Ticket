----  Pick Ticket README ----

**Bonsoir les enfants :**

**Installation :**

Installer php7.1 ainsi que le paquet 7.1-xml (sinon votre composer install ne fonctionnera pas)
Installer le paquet php7.1-mysql
Installer composer          
git clone git@gitlab.com:lucasHoules/Pick_Ticket.git            
composer install (à la racine biensur)

**Exécution de l'application :**

php bin/console server:start = > lance un serveur web configuré de base par le framework
 (super pratique)
 
**Configuration BDD :**

Merci de copier le .env.dist en un ".env" en y remplacant votre mysql user et mysql password.
Si je vois mot de passe passé dans .env.dist je nique des mères !


**Voici les commandes  relatif à la BDD:** 

Création de la bdd = > php bin/console doctrine:database:create 
       
Préparation de la migration => php bin/console make:migration      
Mise à jour de votre "schéma" =>  php bin/console doctrine:migrations:migrate
 
 Concrétement vous utiliserez les 2 premières commandes une fois puis vous enchainerez la 3 ème à chaque mise à jour du schéma..
 
 
 ** Fixtures **
 
 bin/console doctrine:fixtures:load => ça purge la bdd et ça fait des INSERT INTO de nos fakes données ^^
 
 **Commandes Yarn :** 
Installez yarn 
 yarn run encore dev (Compilation des sass et minimification des fichiers CSS/JS concrétement)