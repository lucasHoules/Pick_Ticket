<?php

namespace App\Repository;

use App\Entity\Ticket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ticket|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ticket|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ticket[]    findAll()
 * @method Ticket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TicketRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ticket::class);
    }

    /**
     * Return the list of non closed tickets
     * @return Ticket[]
     */
    public function findNonClosed()
    {
        return $this->baseQueryBuilder()
            ->where('t.state != :closed')
            ->setParameter('closed', Ticket::STATE_CLOSED)
            ->getQuery()
            ->getResult();
    }

    /**
     * Return the list of closed tickets
     * @return Ticket[]
     */
    public function findClosed()
    {
        return $this->baseQueryBuilder()
            ->where('t.state = :closed')
            ->setParameter('closed', Ticket::STATE_CLOSED)
            ->getQuery()
            ->getResult();
    }

    public function findAllWithJoins()
    {
        return $this->baseQueryBuilder()
            ->getQuery()
            ->getResult();
    }

    public function findHighPriorityNotClosed()
    {
        return $this->baseQueryBuilder()
            ->where('t.priority = :high')
            ->andWhere('t.state != :closed')
            ->setParameter('closed', Ticket::STATE_CLOSED)
            ->setParameter('high', Ticket::PRIORITY_HIGH)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $id
     * @return Ticket|null
     */
    public function findByIdWithJoin(int $id)
    {
        return $this->createQueryBuilder('t')
            ->leftJoin('t.ticketUpdates', 'tu')
            ->leftJoin('tu.author', 'a')
            ->addSelect('tu, a')
            ->where('t.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    protected function baseQueryBuilder()
    {
        return $this->createQueryBuilder('t')
            ->leftJoin('t.customer', 'c')
            ->leftJoin('t.project', 'p')
            ->addSelect('c, p')
            ->orderBy('t.updatedAt', 'DESC');
    }

    public function findBySlug($searchTerm)
    {
        return $this->baseQueryBuilder()
            ->where('p.name LIKE :searchterm OR c.name LIKE :searchterm OR t.id LIKE :searchterm OR t.name LIKE :searchterm')
            ->setParameter('searchterm', "%". $searchTerm ."%")
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return Ticket[] Returns an array of Ticket objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ticket
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
