<?php
/**
 * TicketManager.php
 *
 * (c) Loïc Payol <contact@loicpayol.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Manager;


use App\Entity\PriorityUpdate;
use App\Entity\StatusUpdate;
use App\Entity\Ticket;
use App\Entity\TicketUpdate;
use App\Entity\User;
use App\Repository\TicketRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;

/**
 * Manages ticket handling for the users of the application.
 * Contains every use-case implementation.
 * @author Loïc Payol <contact@loicpayol.fr>
 */
class TicketManager
{
    /**
     * The current logged in user.
     * @var User
     */
    private $user;

    /**
     * @var TicketRepository
     */
    private $ticketRepo;

    /**
     * @var ObjectManager
     */
    private $doctrine;

    /**
     * TicketManager constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param TicketRepository $ticketRepo
     */
    public function __construct(TokenStorageInterface $tokenStorage, TicketRepository $ticketRepo, ObjectManager $objectManager)
    {
        if (null === $tokenStorage->getToken()) {
            throw new TokenNotFoundException();
        }

        $this->user = $tokenStorage->getToken()->getUser();
        $this->ticketRepo = $ticketRepo;
        $this->doctrine = $objectManager;
    }

    /**
     * @param string $view
     * @return \App\Entity\Ticket[]
     */
    public function findDashboardTickets(string $view = 'not-closed')
    {
        switch ($view) {
            case 'all':
                return $this->ticketRepo->findAllWithJoins();
            case 'closed':
                return $this->ticketRepo->findClosed();
            case 'high-prio':
                return $this->ticketRepo->findHighPriorityNotClosed();
            case 'not-closed':
            default:
                return $this->ticketRepo->findNonClosed();
        }
    }

    public function search($slug)
    {
        return $this->ticketRepo->findBySlug($slug);
    }

    public function addTicketWithMessage(Ticket $ticket, string $message)
    {
        $ticketUpdate = new TicketUpdate();
        $ticketUpdate->setMessage($message);

        $this->addTicketUpdate($ticket, $ticketUpdate);
    }

    public function addTicketUpdate(Ticket $ticket, TicketUpdate $ticketUpdate)
    {
        $ticket->addTicketUpdate($ticketUpdate);
        $ticketUpdate->setAuthor($this->user);

        $this->doctrine->persist($ticketUpdate->getTicket());
        $this->doctrine->persist($ticketUpdate);
        $this->doctrine->flush();
    }

    public function addStatusUpdate(Ticket $ticket, int $prevStatus)
    {
        $statusUpdate = new StatusUpdate();
        $statusUpdate->setTicket($ticket);
        $statusUpdate->setFrom($prevStatus);
        $statusUpdate->setTo($ticket->getState());

        $this->addTicketUpdate($ticket, $statusUpdate);
    }

    public function addPriorityUpdate(Ticket $ticket, int $prevPriority)
    {
        $priorityUpdate = new PriorityUpdate();
        $priorityUpdate->setTicket($ticket);
        $priorityUpdate->setFrom($prevPriority);
        $priorityUpdate->setTo($ticket->getPriority());

        $this->addTicketUpdate($ticket, $priorityUpdate);
    }
}