<?php

namespace App\Twig;

use Jenssegers\Date\Date;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class RelativeDateTimeExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('relative_date', [$this, 'relativeDate'], ['is_safe' => ['html']]),
        ];
    }

    public function relativeDate(\DateTime $date)
    {
        Date::setLocale('fr'); // TODO : use Symfony config locale
        return (new Date($date->getTimestamp()))->ago();
    }
}
