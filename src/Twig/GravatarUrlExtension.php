<?php

namespace App\Twig;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class GravatarUrlExtension extends AbstractExtension
{

    public function getFunctions(): array
    {
        return [
            new TwigFunction('gravatar_url', [$this, 'gravatarUrl']),
        ];
    }

    public function gravatarUrl(string $email, array $options = [])
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $endpoint = 'https://www.gravatar.com/avatar/';
        $hash = md5(strtolower(trim($email)));
        $options = http_build_query($resolver->resolve($options));

        return "$endpoint/$hash?$options";
    }

    private function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'size' => 60,
            'default' => 'identicon',
            'force_default' => false
        ]);

        $resolver->setAllowedTypes('size', 'int');
        $resolver->setAllowedTypes('force_default', 'bool');
        $resolver->setAllowedValues('default', [
            '404',
            'mp',
            'identicon',
            'monsterid',
            'wavatar',
            'retro',
            'robohash',
            'blank'
        ]);
    }
}
