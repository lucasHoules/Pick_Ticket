<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 15; $i++) {
            $user = new User();
            $user->setUsername($faker->userName);
            $user->setEmail($faker->email);
            $user->setActive($faker->boolean);
            if($i > 8){
                $user->setRoles(["ROLE_DEVELOPPEUR"]);
                $plainPassword = "développeur";
            } else{
                $user->setRoles(["ROLE_RAPPORTEUR"]);
                $plainPassword = "rapporteur";
            }
            $user->setPlainPassword($plainPassword);
            $encoded = $this->encoder->encodePassword($user, $plainPassword);
            $user->setPassword($encoded);
            $manager->persist($user);
        }

        /* admin here */
        $user = new User();
        $user->setUsername("Lucas");
        $user->setEmail("lucas.houles@efrei.net");
        $plainPassword = "admin";
        $user->setRoles(["ROLE_ADMIN"]);
        $user->setPlainPassword($plainPassword);
        $encoded = $this->encoder->encodePassword($user, $plainPassword);
        $user->setPassword($encoded);
        $manager->persist($user);


        $manager->flush();
    }
}