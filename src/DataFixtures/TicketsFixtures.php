<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Project;
use App\Entity\Ticket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class TicketsFixtures extends Fixture
{


    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create('fr_FR');

        $customers = $manager->getRepository(Customer::class)->findAll();
        $projects = $manager->getRepository(Project::class)->findAll();

        for ($i = 0; $i < 250; $i++) {
            $ticket = new Ticket();
            $ticket->setName($faker->colorName);
            $ticket->setPriority($faker->randomElement(Ticket::getValidPriorities()));
            $ticket->setState($faker->randomElement(Ticket::getValidStates()));
            $ticket->setCustomer($faker->optional(0.8)->randomElement($customers));
            $ticket->setProject($faker->optional(0.8)->randomElement($projects));
            $ticket->setCreatedAt($faker->dateTimeBetween('-1 year'));
            $ticket->setUpdatedAt($ticket->getCreatedAt());
            $manager->persist($ticket);
        }


        $manager->flush();
    }



}

