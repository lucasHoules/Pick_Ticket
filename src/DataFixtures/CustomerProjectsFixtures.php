<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class CustomerProjectsFixtures extends Fixture
{


    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 100; $i++) {
            $customer = new Customer();
            $customer->setName($faker->company);
            $customer->setContact($faker->name);
            $project = new Project();
            $project->setName($faker->colorName);
            $project->setDescription($faker->paragraph);
            $project->addCustomer($customer);
            $customer->addProject($project);
            $manager->persist($customer);
            $manager->persist($project);

        }

        $manager->flush();
    }


}

