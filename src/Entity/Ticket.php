<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TicketRepository")
 */
class Ticket
{
    use TimestampableEntity;

    const STATE_OPEN = 1;
    const STATE_IN_WORK = 2;
    const STATE_NEEDS_INFO = 3;
    const STATE_CLOSED = 4;

    const PRIORITY_LOW = 1;
    const PRIORITY_MED = 2;
    const PRIORITY_HIGH = 3;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    private $name;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="change", field="state", value="4")
     * @Assert\DateTime()
     */
    private $closedAt;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     * @Assert\Choice(callback="getValidStates")
     */
    private $state = self::STATE_OPEN;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     * @Assert\Choice(callback="getValidPriorities")
     */
    private $priority = self::PRIORITY_LOW;

    /**
     * @var Project|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $project;

    /**
     * @var Customer|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $customer;

    /**
     * @var TicketUpdate[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\TicketUpdate", mappedBy="ticket")
     * @Assert\Valid
     */
    private $ticketUpdates;

    public function __construct()
    {
        $this->ticketUpdates = new ArrayCollection();
    }

    /**
     * @return int[]
     */
    public static function getValidStates()
    {
        return [
            self::STATE_OPEN,
            self::STATE_IN_WORK,
            self::STATE_NEEDS_INFO,
            self::STATE_CLOSED
        ];
    }

    /**
     * @return int[]
     */
    public static function getValidPriorities()
    {
        return [
            self::PRIORITY_LOW,
            self::PRIORITY_MED,
            self::PRIORITY_HIGH
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime|null
     */
    public function getClosedAt(): ?\DateTime
    {
        return $this->closedAt;
    }

    /**
     * @param \DateTime|null $closedAt
     */
    public function setClosedAt(?\DateTime $closedAt): void
    {
        $this->closedAt = $closedAt;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState(int $state): void
    {
        $this->state = $state;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return Project|null
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project|null $project
     */
    public function setProject(?Project $project): void
    {
        $this->project = $project;
    }

    /**
     * @return Customer|null
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer|null $customer
     */
    public function setCustomer(?Customer $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @return TicketUpdate[]|ArrayCollection
     */
    public function getTicketUpdates()
    {
        return $this->ticketUpdates;
    }

    /**
     * @param TicketUpdate $ticketUpdate
     */
    public function addTicketUpdate(TicketUpdate $ticketUpdate)
    {
        if (!$this->ticketUpdates->contains($ticketUpdate))
            $this->ticketUpdates[] = $ticketUpdate;

        $ticketUpdate->setTicket($this);
    }

    /**
     * @param TicketUpdate $ticketUpdate
     */
    public function removeTicketUpdate(TicketUpdate $ticketUpdate)
    {
        $this->ticketUpdates->removeElement($ticketUpdate);
    }

    public function getName()
    {
        return $this->name;
    }


    public function setName(String $name): void
    {
        $this->name = $name;
    }

}