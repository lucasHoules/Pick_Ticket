<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="Customer", mappedBy="projects", cascade={"all"})
     */
    private $customers;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->customers = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return ArrayCollection|Customer[]
     */
    public function getCustomers(): ArrayCollection
    {
        return $this->customers;
    }

    public function addCustomer(Customer $customer)
    {
        if(!$this->customers->contains($customer))
            $this->customers[] = $customer;

        if(!$customer->getProjects()->contains($this))
            $customer->addProject($this);
    }

    public function removeCustomer(Customer $customer)
    {
        $this->customers->removeElement($customer);
        if ($customer->getProjects()->contains($this)) {
            $customer->removeProject($this);
        }
    }
}
