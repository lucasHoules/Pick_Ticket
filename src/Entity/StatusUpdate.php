<?php
/**
 * MessageUpdate.php
 *
 * (c) Loïc Payol <contact@loicpayol.fr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class StatusUpdate
 * @author Loïc Payol <contact@loicpayol.fr>
 * @ORM\Entity()
 */
class StatusUpdate extends TicketUpdate
{
    /**
     * @var int
     * @ORM\Column(type="smallint", name="from_val")
     * @Assert\Choice(callback="getValidStates")
     */
    private $from;

    /**
     * @var int
     * @ORM\Column(type="smallint", name="to_val")
     * @Assert\Choice(callback="getValidStates")
     */
    private $to;

    public static function getValidStates()
    {
        return Ticket::getValidStates();
    }


    /**
     * @return int
     */
    public function getFrom(): int
    {
        return $this->from;
    }

    /**
     * @param int $from
     */
    public function setFrom(int $from): void
    {
        $this->from = $from;
    }

    /**
     * @return int
     */
    public function getTo(): int
    {
        return $this->to;
    }

    /**
     * @param int $to
     */
    public function setTo(int $to): void
    {
        $this->to = $to;
    }

    public function getUpdateType()
    {
        return 'status_update';
    }
}