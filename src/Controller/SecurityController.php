<?php

namespace  App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthorizationCheckerInterface $authChecker)
    {
        if ($authChecker->isGranted("ROLE_DEVELOPPEUR") || $authChecker->isGranted("ROLE_RAPPORTEUR") || $authChecker->isGranted("ROLE_ADMIN")) {
            return $this->redirect($this->generateUrl('home'));
        }
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/connexion.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }
}