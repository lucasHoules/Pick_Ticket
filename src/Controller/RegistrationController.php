<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session as Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="user_registration")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, AuthorizationCheckerInterface $authChecker, Session $session)
    {
        if ($authChecker->isGranted("ROLE_DEVELOPPEUR") || $authChecker->isGranted("ROLE_RAPPORTEUR") || $authChecker->isGranted("ROLE_ADMIN")) {
            return $this->redirect($this->generateUrl('home'));
        }
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $session->getFlashBag()->add('success', 'Inscription réussie ! Veuillez vous connecter');
            return $this->redirectToRoute('home');
        }

        return $this->render(
            'security/register.twig',
            array('form' => $form->createView())
        );
    }
}