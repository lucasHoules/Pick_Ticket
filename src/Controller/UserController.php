<?php

namespace App\Controller;

use App\Form\UserAddType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\User;
use App\Form\UserType;
use App\Form\UserEditType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\Session as Session;

class UserController extends Controller
{

    /**
     * @Route("/User/list", name="User_list")
     */
    public function listAction(Session $session)
    {

        $Users = $this->getRepository()->findAll();
        return $this->render('User/list.html.twig', [
            'users' => $Users
        ]);
    }

    /**
     * @Route("/User/Delete/{id}", name="User_delete")
     */
    public function deleteAction(ObjectManager $manager, $id, Session $session)
    {
        $user = $this->getRepository()->findOneBy(["id" => $id]);
        $manager->remove($user);
        $manager->flush();
        $session->getFlashBag()->add('danger', 'Supression de l\'utilisateur ' . $user->getUsername() .  ' effectué !');
        return $this->redirectToRoute("User_list");
    }

    /**
     * @Route("User/Edit/{id}", name="User_edit")
     */
    public function editAction(Request $request, ObjectManager $manager, $id)
    {
        $user = $this->getRepository()->findOneBy(["id" => $id]);
        $form = $this->createForm(UserEditType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute("User_list");

        }


        return $this->render('User/edit.html.twig', [
            "form" => $form->createView(),
            'user' => $user
        ]);

    }

    /**
     * @Route("User/Add", name="User_add")
     */
    public function addAction(Request $request, ObjectManager $manager,  UserPasswordEncoderInterface $passwordEncoder, Session $session){

        $user = new User();
        $form = $this->createForm(UserAddType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $manager->persist($user);
            $manager->flush();
            $session->getFlashBag()->add('success', 'Ajout de l\'utilisateur ' . $user->getUsername(). ' effectué !');
            return $this->redirectToRoute("User_list");
        }
        return $this->render('User/add.html.twig', [
            "form" => $form->createView()
        ]);

    }

    /**
     * @Route("/profile", name="User_profile")
     */
    public function profileAction(Request $request, ObjectManager $manager,  UserPasswordEncoderInterface $passwordEncoder, Session $session){

        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $manager->persist($user);
            $manager->flush();
            $session->getFlashBag()->add('success', 'Mise à jour du profil ' . $user->getUsername(). ' effectué !');
            return $this->redirectToRoute("User_profile");

        }

        return $this->render('User/profile.html.twig', [
            "form" => $form->createView(),
            'user' => $user
        ]);

    }



    protected function getRepository(){

        return $this->getDoctrine()->getRepository(User::class);
    }
}
