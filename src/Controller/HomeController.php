<?php

namespace App\Controller;

use App\Manager\TicketManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * Display non closed tickets
     * @Route("/", name="home")
     */
    public function index(TicketManager $ticketManager, Request $request)
    {
        $view = $request->query->get('view', 'not-closed');
        return $this->render('index.twig', [
            'tickets' => $ticketManager->findDashboardTickets($view),
            'view' => $view
        ]);
    }


    /**
     * Search query dashboard
     * @Route("/search", name="search")
     */
    public function searchAction(TicketManager $ticketManager, Request $request)
    {
        $view = $request->query->get('view', 'not-closed');
        $slug =  $request->query->get('srch-term');
        return $this->render('index.twig', [
            'tickets' => $ticketManager->search($slug),
            'view' => $view
        ]);
    }
}
