<?php
/**
 * Created by PhpStorm.
 * User: pc-benoit
 * Date: 09/07/18
 * Time: 14:13
 */

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Customer;
use App\Form\ProjectAddType;
use App\Form\ProjectEditType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session as Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;


class ProjetController extends Controller
{
    /**
     * @Route("/projects", name="gestionProjects")
     */
    public function gestionProjects(Request $request, ObjectManager $manager, Session $session)
    {
        $projects = $this->getRepository()->findAll();
        $project = new Project();
        $form = $this->createForm(ProjectAddType::class, $project);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($project);
            foreach($project->getCustomers() as $customer) {
                $project->addCustomer($customer);
            }
            $manager->flush();
            $session->getFlashBag()->add('success', 'Ajout du projet ' . $project->getName(). ' effectué !');
            return $this->redirectToRoute("gestionProjects");

        }
        return $this->render('projets/projets.twig',[
            'titre'=>'Projets',
            'projects' => $projects,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/projects/Delete/{id}", name="Projects_delete")
     */
    public function deleteAction(ObjectManager $manager, $id, Session $session)
    {
        $project = $this->getRepository()->findOneBy(["id" => $id]);
        $manager->remove($project);
        $manager->flush();
        $session->getFlashBag()->add('danger', 'Supression du projet ' . $project->getName() .  ' effectué !');
        return $this->redirectToRoute("gestionProjects");
    }

    /**
     * @Route("/projects/Edit/{id}", name="Projects_edit")
     */
    public function editAction(Request $request, ObjectManager $manager, $id, Session $session)
    {
        $project = $this->getRepository()->findOneBy(["id" => $id]);
        $form = $this->createForm(ProjectEditType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($project);
            $manager->flush();
            $session->getFlashBag()->add('success', 'Edition du projet ' . $project->getName(). ' effectué !');
            return $this->redirectToRoute("gestionProjects");

        }

        return $this->render('projets/edit.html.twig', [
            "form" => $form->createView(),
            'project' => $project
        ]);

    }
    protected function getRepository(){

        return $this->getDoctrine()->getRepository(Project::class);
    }
}