<?php
/**
 * Created by PhpStorm.
 * User: pc-benoit
 * Date: 10/07/18
 * Time: 09:43
 */

namespace App\Controller;

use App\Entity\Customer;
use App\Form\CustomerAddType;
use App\Form\CustomerEditType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session as Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;


class CustomerController extends Controller
{
    /**
     * @Route("/clients", name="gestionCustomers")
     */
    public function gestionCustomer(Request $request, ObjectManager $manager, Session $session)
    {
        $customers = $this->getRepository()->findAll();
        $customer = new Customer();
        $form = $this->createForm(CustomerAddType::class, $customer);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($customer);
            $manager->flush();
            $session->getFlashBag()->add('success', 'Ajout du client ' . $customer->getName(). ' effectué !');
            return $this->redirectToRoute("gestionCustomers");
        }
        return $this->render('customer/customer.twig',[
            'titre'=>'Client',
            'customersList' => $customers,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/clients/Delete/{id}", name="Clients_delete")
     */
    public function deleteAction(ObjectManager $manager, $id, Session $session)
    {
        $customer = $this->getRepository()->findOneBy(["id" => $id]);
        $manager->remove($customer);
        $manager->flush();
        $session->getFlashBag()->add('danger', 'Supression du client ' . $customer->getName() .  ' effectué !');
        return $this->redirectToRoute("gestionCustomers");
    }

    /**
     * @Route("/clients/Edit/{id}", name="Clients_edit")
     */
    public function editAction(Request $request, ObjectManager $manager, $id, Session $session)
    {
        $customer = $this->getRepository()->findOneBy(["id" => $id]);
        $form = $this->createForm(CustomerEditType::class, $customer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($customer);
            $manager->flush();
            $session->getFlashBag()->add('success', 'Edition du client ' . $customer->getName(). ' effectué !');
            return $this->redirectToRoute("gestionCustomers");

        }

        return $this->render('customer/edit.html.twig', [
            "form" => $form->createView(),
            'customer' => $customer
        ]);

    }


    protected function getRepository(){

        return $this->getDoctrine()->getRepository(Customer::class);
    }

}