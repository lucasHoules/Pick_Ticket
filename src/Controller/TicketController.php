<?php
/**
 * Created by PhpStorm.
 * User: pc-benoit
 * Date: 09/07/18
 * Time: 13:12
 */

namespace App\Controller;

use App\Entity\Ticket;
use App\Entity\TicketUpdate;
use App\Form\PriorityUpdateAddType;
use App\Form\StatusUpdateAddType;
use App\Form\TicketAddType;
use App\Form\TicketUpdateAddType;
use App\Manager\TicketManager;
use App\Repository\TicketRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TicketController extends Controller
{
    /**
     * @Route("/addticket", name="addTicket")
     */
    public function add(Request $request, TicketManager $ticketManager)
    {
        $this->denyAccessUnlessGranted('ROLE_RAPPORTEUR');

        $ticket = new Ticket();
        $form = $this->createForm(TicketAddType::class, $ticket);
        $form->add('Créer le ticket', SubmitType::class, [
            'attr' => [
                'class' => 'btn-primary'
            ]
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ticketManager->addTicketWithMessage($ticket, $form->get('description')->getData());
            $this->addFlash('success', 'Ticket créé.');

            return $this->redirectToRoute('showTicket', ['id' => $ticket->getId()]);
        }


        return $this->render('rapporteur/ajoutTicket.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param int $id
     * @param Request $request
     * @Route("/showticket/{id}", name="showTicket", requirements={"id", "/d+"})
     */
    public function show(int $id, TicketRepository $repository)
    {
        $ticket = $this->findTicket($id, $repository);

        return $this->render('ticket/show.html.twig', [
            'ticket' => $ticket,
            'form' => [
                'ticket_update' => $this->createTicketUpdateForm($ticket)->createView(),
                'status_update' => $this->createStatusUpdateForm($ticket)->createView(),
                'priority_update' => $this->createPriorityUpdateForm($ticket)->createView()
            ]
        ]);
    }

    /**
     * @Route("/showticket/{id}/addTicketUpdate", name="addTicketUpdate", requirements={"id", "/d+"},methods={"POST"})
     */
    public function addTicketUpdate(int $id, TicketRepository $repository, TicketManager $manager, Request $request)
    {
        $ticket = $this->findTicket($id, $repository);
        $form = $this->createTicketUpdateForm($ticket);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            /** @var TicketUpdate $ticketUpdate */
            $ticketUpdate = $form->getData();
            $manager->addTicketUpdate($ticket, $ticketUpdate);
            $this->addFlash('success', 'Message créé.');

            return $this->redirectToRoute('showTicket', ['id' => $ticket->getId()]);
        }

        return $this->render('ticket/show.html.twig', [
            'ticket' => $ticket,
            'form' => [
                'ticket_update' => $form->createView(),
                'status_update' => $this->createStatusUpdateForm($ticket)->createView(),
                'priority_update' => $this->createPriorityUpdateForm($ticket)->createView()
            ]
        ]);
    }

    /**
     * @Route("/showticket/{id}/addStatusUpdate", name="addStatusUpdate", requirements={"id", "/d+"},methods={"POST"})
     */
    public function addStatusUpdate(int $id, TicketRepository $repository, TicketManager $manager, Request $request)
    {
        $ticket = $this->findTicket($id, $repository);

        $prevStatus = $ticket->getState();

        $form = $this->createStatusUpdateForm($ticket);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            if ($prevStatus === $ticket->getState()) {
                $this->addFlash('warning', 'Le status n\'a pas changé, la mise à jour n\'a pas été enregistrée.');
            } else {

                $manager->addStatusUpdate($ticket, $prevStatus);
                $this->addFlash('success', 'Status mis à jour.');

                return $this->redirectToRoute('showTicket', ['id' => $ticket->getId()]);
            }
        }

        return $this->render('ticket/show.html.twig', [
            'ticket' => $ticket,
            'form' => [
                'ticket_update' => $this->createTicketUpdateForm($ticket)->createView(),
                'status_update' => $form->createView(),
                'priority_update' => $this->createPriorityUpdateForm($ticket)->createView()
            ]
        ]);
    }

    /**
     * @Route("/showticket/{id}/addPriorityUpdate", name="addPriorityUpdate", requirements={"id", "/d+"},methods={"POST"})
     */
    public function addPriorityUpdate(int $id, TicketRepository $repository, TicketManager $manager, Request $request)
    {
        $ticket = $this->findTicket($id, $repository);

        $prevPriority = $ticket->getPriority();

        $form = $this->createPriorityUpdateForm($ticket);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            if ($prevPriority === $ticket->getPriority()) {
                $this->addFlash('warning', 'La priorité n\'a pas changée, la mise à jour n\'a pas été enregistrée.');
            } else {

                $manager->addPriorityUpdate($ticket, $prevPriority);
                $this->addFlash('success', 'Priorité mise à jour.');

                return $this->redirectToRoute('showTicket', ['id' => $ticket->getId()]);
            }
        }

        return $this->render('ticket/show.html.twig', [
            'ticket' => $ticket,
            'form' => [
                'ticket_update' => $this->createTicketUpdateForm($ticket)->createView(),
                'status_update' => $this->createStatusUpdateForm($ticket)->createView(),
                'priority_update' => $form->createView(),
            ]
        ]);
    }

    /**
     * @param Ticket $ticket
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createTicketUpdateForm(Ticket $ticket)
    {
        $form = $this->createForm(TicketUpdateAddType::class, new TicketUpdate(), [
            'action' => $this->generateUrl('addTicketUpdate', ['id' => $ticket->getId()])
        ]);

        $this->addSubmit($form);
        return $form;
    }

    private function createStatusUpdateForm(Ticket $ticket)
    {
        $form = $this->createForm(StatusUpdateAddType::class, $ticket, [
            'action' => $this->generateUrl('addStatusUpdate', ['id' => $ticket->getId()])
        ]);

        $this->addSubmit($form);
        return $form;
    }

    private function createPriorityUpdateForm(Ticket $ticket)
    {
        $form = $this->createForm(PriorityUpdateAddType::class, $ticket, [
            'action' => $this->generateUrl('addPriorityUpdate', ['id' => $ticket->getId()])
        ]);

        $this->addSubmit($form);
        return $form;
    }

    private function addSubmit(FormInterface $form)
    {
        $form->add('Envoyer', SubmitType::class, [
            'attr' => [
                'class' => 'btn-primary'
            ]
        ]);
    }

    /**
     * @param int $id
     * @param TicketRepository $repository
     * @return Ticket|null
     */
    private function findTicket(int $id, TicketRepository $repository)
    {
        $ticket = $repository->findByIdWithJoin($id);
        if (null === $ticket) {
            throw $this->createNotFoundException();
        }
        return $ticket;
    }
}