<?php

namespace App\Form;

use App\Entity\Customer;
use App\Entity\Project;
use App\Entity\Ticket;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TicketAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('state', ChoiceType::class, [
                'choices' => [
                    'Ouvert' => Ticket::STATE_OPEN,
                    'En cours' => Ticket::STATE_IN_WORK,
                    'Besoin d\'informations' => Ticket::STATE_NEEDS_INFO,
                    'Fermé' => Ticket::STATE_CLOSED
                ],
                'label' => 'Statut'
            ])
            ->add('priority', ChoiceType::class, [
                'choices' => [
                    'Faible' => Ticket::PRIORITY_LOW,
                    'Moyenne' => Ticket::PRIORITY_MED,
                    'Haute' => Ticket::PRIORITY_HIGH
                ],
                'label' => "Priorité"
            ])
            ->add('project', EntityType::class, [
                'class' => Project::class,
                'choice_label' => 'name',
                'label' => 'Projet'
            ])
            ->add('customer', EntityType::class, [
                'class' => Customer::class,
                'choice_label' => 'name',
                'label' => 'Client'
            ])
            ->add('description', TextareaType::class, [
                'mapped' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ticket::class,
        ]);
    }
}
